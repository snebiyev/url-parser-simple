package az.techlab.runner;

import az.techlab.exception.InvalidUrlException;
import az.techlab.factory.ParserFactory;
import az.techlab.helper.Parser;

import java.io.IOException;

public class Runner {
    // Use Factory Design Patter to get Parser instance
    private static final Parser parser = ParserFactory.getParser("URL");

    public static String run(String url) {
        try {
            return parser.parseText(url);
        } catch (InvalidUrlException invalidUrlException) {
            return invalidUrlException.getMessage() + ": " + url;
        } catch (IOException httpStatusException) {
            return "Cannot parse text from url: " + url;
        }
    }
}
