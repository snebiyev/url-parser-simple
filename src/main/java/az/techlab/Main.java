package az.techlab;

import az.techlab.runner.Runner;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Enter url (or 'q' to exit): ");
            String url = scanner.nextLine();
            if (url.equals("q")) {
                System.out.println("Bye!");
                break;
            }
            System.out.println(Runner.run(url));
        }
    }
}
