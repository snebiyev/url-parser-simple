package az.techlab.factory;

import az.techlab.helper.Parser;
import az.techlab.helper.UrlParser;

public class ParserFactory {
    public static Parser getParser(String parserType) {
        if (parserType == null) {
            throw new IllegalArgumentException("Parser type is null");
        }
        if (parserType.equalsIgnoreCase("URL")) {
            return new UrlParser();
        } else if (parserType.equalsIgnoreCase("TEXT")) {
            throw new IllegalArgumentException("Text parser is not implemented yet");
        }
       throw new IllegalArgumentException("Parser is not implemented yet");
    }
}
