package az.techlab.helper;

public class UrlValidator implements Validator {
    @Override
    public boolean validateUrl(String url) {
        if (url == null || url.isEmpty()) {
            return false;
        }
        // Should be a valid HTTP or HTTPS URL
        return url.matches("^(https?)://[-a-zA-Z\\d+&@#/%?=~_|!:,.;]*[-a-zA-Z\\d+&@#/%=~_|]");
    }
}
