package az.techlab.helper;

import az.techlab.exception.InvalidUrlException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class UrlParser implements Parser {
    Validator validator = new UrlValidator();
    @Override
    public String parseText(String url) throws IOException {
        if (!validator.validateUrl(url)) {
            throw new InvalidUrlException("Invalid URL");
        }
        Document doc = Jsoup.connect(url).get();
        return doc.getAllElements().text();
    }

}
