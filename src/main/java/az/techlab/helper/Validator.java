package az.techlab.helper;

public interface Validator {
    boolean validateUrl(String url);
}
