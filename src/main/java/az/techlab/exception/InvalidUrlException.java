package az.techlab.exception;

public class InvalidUrlException extends IllegalArgumentException {
    private static final long serialVersionUID = 5530733754303506170L;

    public InvalidUrlException(String message) {
        super(message);
    }
}
