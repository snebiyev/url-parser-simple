package az.techlab.runner;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

@DisplayName("Test for Runner class")
class RunnerTest {

    @Test
    @DisplayName("Test for invalid url")
    void runGivenInvalidUrlShouldReturnInvalidURLText() {
        String url = "htp://www.google.com";
        String expected = "Invalid URL: " + url;
        String actual = Runner.run(url);
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test for not 200 status returned url")
    void runGivenInvalidUrlShouldReturnCannotParseText() {
        String url = "https://www.google.com/invalid";
        String expected = "Cannot parse text from url: " + url;
        String actual = Runner.run(url);
        assertEquals(expected, actual);
    }

    @Test
    @DisplayName("Test for valid url")
    void runGivenInvalidUrlShouldNotReturnExceptionMessages() {
        String url = "https://www.google.com";
        String[] expectedText = {"Cannot parse text from url: " + url, "Invalid URL: " + url};
        String actual = Runner.run(url);
        assertFalse(Arrays.asList(expectedText).contains(actual));
    }
}
