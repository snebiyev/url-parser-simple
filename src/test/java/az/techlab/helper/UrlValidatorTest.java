package az.techlab.helper;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
@DisplayName("Test for UrlValidator class")
class UrlValidatorTest {

    static Validator validator = new UrlValidator();
    @BeforeAll
    static void setUp() {
        assertNotNull(validator);
    }


    @Test
    @DisplayName("Test for invalid url")
    void validateUrlShouldReturnFalse() {
        assertFalse(validator.validateUrl("ht://www.google.com"));
    }

    @Test
    @DisplayName("Test for valid url")
    void validateUrlShouldReturnTrue() {
        assertTrue(validator.validateUrl("https://www.google.com"));
    }
}
