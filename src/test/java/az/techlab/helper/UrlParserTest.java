package az.techlab.helper;

import az.techlab.exception.InvalidUrlException;
import az.techlab.factory.ParserFactory;
import org.jsoup.HttpStatusException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test for UrlParser class")
class UrlParserTest {
    static Parser parser = ParserFactory.getParser("URL");

    @BeforeAll
    static void setUp() {
        assertNotNull(parser);
    }

    @Test
    @DisplayName("Test for valid url")
    void parseTextShouldNotThrowException() {
        assertDoesNotThrow(() -> parser.parseText("https://www.google.com"));
    }

    @Test
    @DisplayName("Test for not found url")
    void parseTextShouldThrowHttpStatusException() {
        assertThrows(HttpStatusException.class, () -> parser.parseText("https://www.google.com/invalid"));
    }

    @Test
    @DisplayName("Test for invalid url")
    void parseTextShouldThrowInvalidUrlException() {
        assertThrows(InvalidUrlException.class, () -> parser.parseText("ht://www.google.com/invalid"));
    }

    @Test
    @DisplayName("Test for null url")
    void parseTextShouldThrowInvalidUrlExceptionForNullUrl() {
        assertThrows(InvalidUrlException.class, () -> parser.parseText(null));
    }

    @Test
    @DisplayName("Test for empty url")
    void parseTextShouldThrowInvalidUrlExceptionForEmptyUrl() {
        assertThrows(InvalidUrlException.class, () -> parser.parseText(""));
    }

    @Test
    @DisplayName("Test for valid url #1")
    void parseTextShouldReturnText() throws IOException {
        String textContent = parser.parseText("https://www.google.com");
        assertNotNull(textContent);
    }

    @Test
    @DisplayName("Test for valid url #2")
    void parseTextShouldReturnText2() throws IOException {
        String textContent = parser.parseText("https://www.tvblog.it/post/1681999/valerio-fabrizio-salvatori-gli-inseparabili-chi-sono-pechino-express-2020");
        assertNotNull(textContent);
    }

    @Test
    @DisplayName("Test for url containing text")
    void parseTextShouldContainString() throws IOException {
        String textContent = parser.parseText("https://edition.cnn.com/");
        assertTrue(textContent.contains("CNN"));
    }
}
